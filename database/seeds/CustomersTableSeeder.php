<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * customer's table seeder.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'name' => 'Juan Velasquez',
            'phone' => '+502 5024 6895',
            'dpi' => '2102354625987',
        ]);
    }
}
