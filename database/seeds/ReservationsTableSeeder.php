<?php

use Illuminate\Database\Seeder;

class ReservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * reservation's table seeder.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->insert([
            'start_date' => '2019-02-15',
            'end_date' => '2019-02-16',
            'is_active' => '1',
            'room_id' => '1',
            'customer_id' => '1', 
        ]);
    }
}
