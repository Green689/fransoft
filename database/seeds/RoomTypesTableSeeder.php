<?php

use Illuminate\Database\Seeder;

class RoomTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * roomtype's table seeder.
     *
     * @return void
     */
    public function run()
    {
        $roomType = [
            [
                'name' => '1 cama',
                'description' => 'habitacion individual de una cama',
            ],[
                'name' => '2 camas',
                'description' => '',
            ],[
                'name' => '3 camas',
                'description' => '',
            ],[
                'name' => '4 camas',
                'description' => '',
            ],[
                'name' => '5 cama',
                'description' => '',
            ],[
                'name' => '6 cama',
                'description' => 'habitacion familiar',
            ],
        ];

        DB::table('room_types')->insert($roomType);
    }
}
