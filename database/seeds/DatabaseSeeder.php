<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * 
     * runs all seeder.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoomTypesTableSeeder::class,
            StatusesTableSeeder::class,
            CustomersTableSeeder::class,
            RoomsTableSeeder::class,
            ReservationsTableSeeder::class,
        ]);

    }
}
