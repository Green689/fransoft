<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * status' table seeder.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            [
                'name' => 'Disponible',
                'description' => '',
            ],
            [
                'name' => 'Mantenimiento',
                'description' => '',
            ], 
            [
                'name' => 'Ocupado',
                'description' => '',
            ],
        ];

        DB::table('statuses')->insert($status);
    }
}
