<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * Room's table seeder.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
            'name' => 'N01',
            'type_id' => '1',
            'status_id' => '1',
        ]);
    }
}
