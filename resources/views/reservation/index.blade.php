<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Reservaciones</title>
</head>

<body>
    <nav class="navbar is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Habitaciones
                </a>
                <a class="navbar-item" href="/customers/">
                    Clientes
                </a>
                <a class="navbar-item" href="/reservations/">
                    Reservaciones
                </a>
            </div>
            <div class="navbar-end">
                <div class="buttons">
                    <a class="button is-white" onclick="location.href='{{ url('/logout') }}'">logout</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="columns">
        <div class="column is-one-quarter"></div>
        <div class="column">
            <div class='columns' style="margin-top: 10px">
                <div class="column"></div>
                <div class="column"></div>
                <div class="column">
                    <button class="button is-success" onclick="location.href='/reservations/create'">Nueva Reservación</button>
                </div>
            </div>

            <table class="table is-fullwidth">
                <thead>
                    <tr>
                        <th>Número de reservacion</th>
                        <th>Fecha de inicio</th>
                        <th>Fecha final</th>
                        <th>Estado</th>
                        <th>Habitación</th>
                        <th>Cliente</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reservations as $reservation)
                    <tr>
                        <td>{{ $reservation->id }}</td>
                        <td>{{ $reservation->start_date }}</td>
                        <td>{{ $reservation->end_date }}</td>

                        @if($reservation->is_active =='1')
                        <td>Activa</td>
                        @else
                        <td>Anulada</td>
                        @endif

                        @foreach($rooms as $room)
                        @if($reservation->room_id == $room->id)
                        <td>{{ $room->name }}</td>
                        @endif
                        @endforeach

                        @foreach($customers as $customer)
                        @if($reservation->customer_id == $customer->id)
                        <td>{{ $customer->name }}</td>
                        @endif
                        @endforeach

                        <td><a href="/reservations/{{ $reservation->id }}/edit">Editar</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</body>

</html>
