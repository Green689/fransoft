<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Editar reservación</title>
</head>

<body>
    <nav class="navbar is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Habitaciones
                </a>
                <a class="navbar-item" href="/customers/">
                    Clientes
                </a>
                <a class="navbar-item" href="/reservations/">
                    Reservaciones
                </a>
            </div>
            <div class="navbar-end">
                <div class="buttons">
                    <a class="button is-white" onclick="location.href='{{ url('/logout') }}'">logout</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="columns">
        <div class="column is-one-quarter"></div>
        <div class="column">
            <h1 class="title is-2">Editar Reservación</h1>
            <form action="/reservations/{{ $reservation->id }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="field">
                    <label class="label">Fecha de inicio</label>
                    <div class="control">
                        <input name='start_date' value='{{ $reservation->start_date }}' class="input" type="date" required>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Fecha final</label>
                    <div class="control">
                        <input name='end_date' value='{{ $reservation->end_date }}' class="input" type="date" required>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Estado</label>
                    <div class="control">
                        <div class="select">
                            <select name='is_active'>
                                @if($reservation->is_active == true)
                                <option value='1'>Activa</option>
                                <option value="0">Anulada</option>
                                @elseif($reservation->is_active == false)
                                <option value="0">Anulada</option>
                                <option value='1'>Activa</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Habitación</label>
                    <div class="control">
                        <div class="select">
                            <select name='room_id'>
                                @foreach($rooms as $room)
                                @if($reservation->room_id == $room->id)
                                <option value='{{ $room->id }}'>{{ $room->name }}</option>
                                @endif
                                @endforeach
                                @foreach($rooms as $room)
                                @if($room->status_id == 1)
                                <option value='{{ $room->id }}'>{{ $room->name }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Cliente</label>
                    <div class="control">
                        <div class="select">
                            <select name='customer_id' disabled>
                                @foreach($customers as $customer)
                                @if($reservation->customer_id == $customer->id)
                                <option value='{{ $customer->id }}'>{{ $customer->name }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="control">
                    <button class="button is-link">Guardar</button>
                </div>
            </form>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</body>

</html>
