<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Nueva Reservación</title>
</head>

<body>
    <nav class="navbar is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Habitaciones
                </a>
                <a class="navbar-item" href="/customers/">
                    Clientes
                </a>
                <a class="navbar-item" href="/reservations/">
                    Reservaciones
                </a>
            </div>
            <div class="navbar-end">
                <div class="buttons">
                    <a class="button is-white" onclick="location.href='{{ url('/logout') }}'">logout</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="columns">
        <div class="column is-one-quarter"></div>
        <div class="column">
            <h1 class="title is-2">Nueva Reservación</h1>
            <form action="/reservations/" method="POST">
                @csrf
                <div class="field">
                    <label class="label">Fecha de inicio</label>
                    <div class="control">
                        <input name='start_date' class="input" type="date" required>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Fecha final</label>
                    <div class="control">
                        <input name='end_date' class="input" type="date" required>
                    </div>
                </div>
                <input hidden type="text" value="1" name="is_active">
                <div class="field">
                    <label class="label">Habitación</label>
                    <div class="control">
                        <div class="select">
                            <select name='room_id'>
                                @foreach($rooms as $room)
                                @if($room->status_id == 1)
                                <option value='{{ $room->id }}'>{{ $room->name }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Cliente</label>
                    <div class="control">
                        <div class="select">
                            <select name='customer_id'>
                                @foreach($customers as $customer)
                                <option value='{{ $customer->id }}'>{{ $customer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control">
                    <button class="button is-link">Guardar</button>
                </div>
            </form>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</body>

</html>
