<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Clientes</title>
</head>

<body>
    <nav class="navbar is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Habitaciones
                </a>
                <a class="navbar-item" href="/customers/">
                    Clientes
                </a>
                <a class="navbar-item" href="/reservations/">
                    Reservaciones
                </a>
            </div>
            <div class="navbar-end">
                <div class="buttons">
                    <a class="button is-white" onclick="location.href='{{ url('/logout') }}'">logout</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="columns">
        <div class="column is-one-quarter"></div>
        <div class="column">
            <h1 class="title is-2">{{ $customer->name }}</h1>
            <div class="columns">
                <div class="column">
                    <div>Teléfono: {{ $customer->phone }}</div>
                    <div>DPI: {{ $customer->dpi }}</div>
                </div>
                <div class="column">
                </div>
                <div class="column">
                    <button class="button is-info" style="margin-left: 70%" onclick='location.href="/customers/{{ $customer->id }}/edit"'>Editar</button>
                </div>
            </div>

            <hr>

            <table class="table is-fullwidth">
                <thead>
                    <tr>
                        <th>Numero de reservación</th>
                        <th>Fecha de inicio</th>
                        <th>Fecha final</th>
                        <th>Estado</th>
                        <th>Habitación</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reservations as $reservation)
                    @foreach($reservation as $res)
                    <tr>
                        <td>{{ $res->id }}</td>
                        <td>{{ $res->start_date }}</td>
                        <td>{{ $res->end_date }}</td>

                        @if($res->is_active =='1')
                        <td>Activa</td>
                        @else
                        <td>Anulada</td>
                        @endif

                        @foreach($rooms as $room)
                        @if($res->room_id == $room->id)
                        <td>{{ $room->name }}</td>
                        @endif
                        @endforeach
                    </tr>
                    @endforeach
                    @endforeach
                </tbody>
            </table>

        </div>
        <div class="column is-one-quarter"></div>
    </div>
</body>

</html>
