<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Clientes</title>
</head>

<body>
    <nav class="navbar is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Habitaciones
                </a>
                <a class="navbar-item" href="/customers/">
                    Clientes
                </a>
                <a class="navbar-item" href="/reservations/">
                    Reservaciones
                </a>
            </div>
            <div class="navbar-end">
                <div class="buttons">
                    <a class="button is-white" onclick="location.href='{{ url('/logout') }}'">logout</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="columns">
        <div class="column is-one-quarter"></div>
        <div class="column">
            <div class='columns' style="margin-top: 10px">
                <div class="column"></div>
                <div class="column"></div>
                <div class="column">
                    <button class="button is-success" onclick="location.href='/customers/create'">Nuevo Cliente</button>
                </div>
            </div>

            <table class="table is-fullwidth">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>DPI</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $customer)
                    <tr>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->phone }}</td>
                        <td>{{ $customer->dpi }}</td>
                        <td><a href="/customers/{{ $customer->id }}">Detalle</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</body>

</html>
