<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Habitaciones</title>
</head>

<body>
    <nav class="navbar is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Habitaciones
                </a>
                <a class="navbar-item" href="/customers/">
                    Clientes
                </a>
                <a class="navbar-item" href="/reservations/">
                    Reservaciones
                </a>
            </div>
            <div class="navbar-end">
                <div class="buttons">
                    <button class="button is-white" onclick="location.href='{{ url('/logout') }}'">logout</button>
                </div>
            </div>
        </div>
    </nav>
    <div class="columns">
        <div class="column is-one-quarter"></div>
        <div class="column">
            <div class='columns' style="margin-top: 10px">
                <div class="column"></div>
                <div class="column"></div>
                <div class="column">
                    <button class="button is-success" onclick="location.href='/create'">Nueva habitación</button>
                </div>
            </div>

            <table class="table is-fullwidth">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo de habitación</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rooms as $room)
                    @foreach ($room as $rom)

                    @if($rom->status_id=='1')
                    <tr class='is-selected'>
                        @else
                    <tr>
                        @endif

                        <td>{{$rom->name}}</td>
                        <td>
                            @foreach($roomTypes as $type)
                            @if($rom->type_id==$type->id)
                            {{$type->name}}
                            @endif
                            @endforeach
                        </td>
                        <td>
                            @foreach($statuses as $status)
                            @if($rom->status_id==$status->id)
                            {{$status->name}}
                            @endif
                            @endforeach
                        </td>
                        <td><a href="/room/edit/{{ $rom->id }}">Editar</a></td>
                    </tr>
                    @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</body>

</html>
