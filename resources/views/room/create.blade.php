<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Nueva Habitación</title>
</head>

<body>
    <nav class="navbar is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Habitaciones
                </a>
                <a class="navbar-item" href="/customers/">
                    Clientes
                </a>
                <a class="navbar-item" href="/reservations/">
                    Reservaciones
                </a>
            </div>
            <div class="navbar-end">
                <div class="buttons">
                    <a class="button is-white" onclick="location.href='{{ url('/logout') }}'">logout</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="columns">
        <div class="column is-one-quarter"></div>
        <div class="column">
            @if(isset($success))
            <div class="notification is-danger">
                <button class="delete"></button>
                {{$success}}
            </div>
            @endif
            <h1 class="title is-2">Nueva habitación</h1>
            <form action="/" method="POST">
                @csrf
                <div class="field">
                    <label class="label">Nombre</label>
                    <div class="control">
                        <input name='name' class="input" type="text" placeholder="e.g N01" required>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Cantidad de camas</label>
                    <div class="control">
                        <div class="select">
                            <select name='type_id'>
                                @foreach($roomTypes as $roomType)
                                <option value='{{ $roomType->id }}'>{{ $roomType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Estado</label>
                    <div class="control">
                        <div class="select">
                            <select name='status_id'>
                                @foreach($statuses as $status)
                                <option value='{{ $status->id }}'>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control">
                    <button class="button is-link">Guardar</button>
                </div>
            </form>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</body>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
            $notification = $delete.parentNode;
            $delete.addEventListener('click', () => {
                $notification.parentNode.removeChild($notification);
            });
        });
    });
</script>

</html>
