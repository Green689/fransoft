<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Editar habitación</title>
</head>

<body>
    <nav class="navbar is-light" role="navigation" aria-label="main navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Habitaciones
                </a>
                <a class="navbar-item" href="/customers/">
                    Clientes
                </a>
                <a class="navbar-item" href="/reservations/">
                    Reservaciones
                </a>
            </div>
            <div class="navbar-end">
                <div class="buttons">
                    <a class="button is-white" onclick="location.href='{{ url('/logout') }}'">logout</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="columns">
        <div class="column is-one-quarter"></div>
        <div class="column">
            <h1 class="title is-2">Editar habitación</h1>
            <form action="/room/{{ $room->id }}" method="POST">
                @csrf
                @method('HEAD')
                <div class="field">
                    <label class="label">Nombre</label>
                    <div class="control">
                        <input name='name' value="{{ $room->name }}" class="input" type="text" placeholder="e.g N01" required>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Cantidad de camas</label>
                    <div class="control">
                        <div class="select">
                            <select name='type_id'>
                                @foreach($roomTypes as $roomType)
                                @if($room->type_id == $roomType->id)
                                <option value='{{ $roomType->id }}'>{{ $roomType->name }}</option>
                                @endif
                                @endforeach
                                @foreach($roomTypes as $roomType)
                                <option value='{{ $roomType->id }}'>{{ $roomType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label class="label">Estado</label>
                    <div class="control">
                        <div class="select">
                            <select name='status_id'>
                                @foreach($statuses as $status)
                                @if($room->status_id == $status->id)
                                <option value='{{ $status->id }}'>{{ $status->name }}</option>
                                @endif
                                @endforeach
                                @foreach($statuses as $status)
                                <option value='{{ $status->id }}'>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control">
                    <button class="button is-link">Guardar</button>
                </div>
            </form>
        </div>
        <div class="column is-one-quarter"></div>
    </div>
</body>

</html>
