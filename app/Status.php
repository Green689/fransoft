<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Status's model.
 */
class Status extends Model
{
    protected $fillable = ['name', 'description'];

    public function rooms()
    {
        return $this->hasMany('App\Room');
    }
}
