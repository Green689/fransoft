<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Room's model.
 */
class Room extends Model
{
    protected $fillable = ['name', 'type_id', 'status_id'];

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function roomType()
    {
        return $this->belongsTo('App\RoomType');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
