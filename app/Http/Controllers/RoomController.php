<?php

namespace App\Http\Controllers;

use App\Room;
use App\Status;
use App\RoomType;
use App\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
    /**
     * function to filter login
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * 
     * show rooms' list.
     * 
     * Muesta la lista de habitaciones agrupadas de acuerdo a su estado,
     * y si la fecha actual es igual a la de la fecha final de una reservacion
     * actualiza el estado de la habitacion. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reservation::all();
        foreach($reservations as $reservation){
            if($reservation->end_date == now()){
                DB::table('rooms')->where('id', $reservation->room_id)->update(['status_id'=>1]);
            };
        };
        $roomTypes = RoomType::all();
        $statuses =  Status::all();
        $rooms = Room::all()->groupBy('status_id');
        return view('room/index', compact('rooms', 'roomTypes', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     * 
     * show room's create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::all();
        $roomTypes = RoomType::all();
        return view('room/create', compact('statuses', 'roomTypes'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * saves room's info in the DB.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Room::where('name', '=', $request->name)->count() > 0){
            $success = 'Ese nombre de habitacion ya existe';
            $statuses = Status::all();
            $roomTypes = RoomType::all();
            return view('room/create', compact('success', 'statuses', 'roomTypes'));
        }else{
            Room::create($request->all());
            return redirect('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * 
     * show room's editing form.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        $statuses = Status::all();
        $roomTypes = RoomType::all();
        return view('room/edit', compact('room', 'statuses', 'roomTypes'));
    }

    /**
     * Update the specified resource in storage.
     * 
     * saves in DB the info updated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $room->update(request()->all());
        return redirect('/');
    }
}
