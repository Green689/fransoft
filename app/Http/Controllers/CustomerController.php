<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Reservation;
use App\Room;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * function to filter login
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     * 
     *  Get customers' list and show it in a view.
     * 
     *  Obtiene la lista de clientes existentes, y la muestra en la view de customer.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('customer/index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     * 
     * redirect customer's form view.
     * 
     * redirige a la vista que contiene el formulario para crear un nuevo cliente. 
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer/create');
    }

    /**
     * Store a newly created resource in storage.
     * 
     * saves info in DB and redirect to customer's index.
     * 
     * Guarda la informacion en ela base de datos y redirecciona a el index de customer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Customer::where('dpi', '=', $request->dpi)->count() > 0) {
            $success = 'Ese dpi ya existe.';
            return view('customer/create', compact('success'));
        } else {
            Customer::create($request->all());
            return redirect('customers/');
        } 
    }

    /**
     * Display the specified resource.
     * 
     * show customer's detail and her/his reservations.
     * 
     * muestra el detalle del cliente, y sus reservaciones anteriores.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        
        $reservations = Reservation::all()->where('customer_id', '=', $customer->id)->groupBy('is_active');
        $rooms = Room::all();

        return view('customer/show', compact('customer', 'reservations', 'rooms'));
    }

    /**
     * Show the form for editing the specified resource.
     * 
     * redirects customer's edit view
     * 
     * redirige a la vista de editar clilente.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('customer/edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     * 
     * updates customer info in DB.
     * 
     * actualiza la informacion del cliente en la base de datos. 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->update(request()->all());
        return redirect()->action(
            'CustomerController@show',
            ['customer' => $customer->id]
        );
    }
}
