<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Customer;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReservationController extends Controller
{
    /**
     * function to filter login
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * 
     * show reservation's list.
     * 
     * Muestra la lista de reservaciones de la mas actual a la mas antigua.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::all();
        $customers = Customer::all();
        $reservations = Reservation::orderBy('id', 'DESC')->get();
        return view('reservation/index', compact('reservations', 'rooms', 'customers'));
    }

    /**
     * Show the form for creating a new resource.
     * 
     * show reservation's create form.
     * 
     * Muestra la vista de formulario de reservacion de create. 
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = Room::all();
        $customers = Customer::all();
        return view('reservation/create', compact('rooms','customers'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * saves reservation's info in the DB.
     * 
     * Guarda la informacion de reservacion en la base de datos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Reservation::create($request->all());
        DB::table('rooms')->where('id', $request->room_id)->update(['status_id' => 3]);
        return redirect('reservations/');
    }

    /**
     * Show the form for editing the specified resource.
     * 
     * show reservation's edit view.
     * 
     * muestra la vista de edicion de reservacion.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        $rooms = Room::all();
        $customers = Customer::all();
        return view('reservation/edit', compact('rooms', 'customers', 'reservation'));
    }

    /**
     * Update the specified resource in storage.
     * 
     * saves reservation info in th DB after updating.
     * 
     * guarda la informacion actualizada de reservacion en la base de datos,
     * y modifica la habitacion correspondiente.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        $reservation->update(request()->all());
        if($reservation->is_active == 0){
            DB::table('rooms')->where('id', $reservation->room_id)->update(['status_id' => 1]);
        }elseif($reservation->is_active == 1){
            DB::table('rooms')->where('id', $reservation->room_id)->update(['status_id' => 3]);
        }
        return redirect('reservations/');
    }
}
