<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * RoomType's model.
 */
class RoomType extends Model
{
    protected $fillable = ['name', 'description'];

    public function rooms()
    {
        return $this->hasMany('App\Room');
    }
}
