<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Reservation's model.
 */
class Reservation extends Model
{
    protected $fillable = ['start_date', 'end_date', 'is_active' ,'room_id', 'customer_id'];

    public function Room()
    {
        return $this->belongsTo('App\Room');
    }
    
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

}
