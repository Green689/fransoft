<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Customer's model.
 */
class Customer extends Model
{
    protected $fillable = ['name','phone','dpi'];

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }
}
